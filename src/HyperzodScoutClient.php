<?php

namespace Hyperzodtech\HyperzodScoutClient;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;
use Exception;

class HyperzodScoutClient
{
    protected $apiKey = null;
    protected $projectId = null;

    protected $query = null;
    protected $ip = null;
    protected $meta = null;

    protected $httpUserAgent = null;
    protected $deviceDetect = null;

    protected $env = 'local'; // live, dev, local
    protected $debug = false;

    public function __construct($apiKey=null, $projectId=null)
    {
        if (!is_null($apiKey))
            $this->setApiKey($apiKey);

        if (!is_null($projectId))
            $this->setProjectId($projectId);
    }

    public function send()
    {
        if (empty($this->apiKey))
            die("API Key not set.");

        if (empty($this->projectId))
            die("Project id not set.");

        if (is_null($this->httpUserAgent))
            $this->httpUserAgent = $_SERVER['HTTP_USER_AGENT'];

        $this->deviceDetect = new DeviceDetector($this->httpUserAgent);
        $this->deviceDetect->parse();

        try {

            $curl = curl_init();

            $post = array(
                'project_id' => $this->projectId,
                'search' => $this->query,
                'platform' => $this->getPlatform(),
                'os' => $this->getOs(),
                'browser' => $this->getBrowser(),
                'ip' => $this->getIp(),
            );

            if (count($this->meta)) {
                foreach ($this->meta as $key => $value) {
                    $post["meta[" . $key . "]"] = $value;
                }
            }

            if ($this->debug) {
                var_dump($post);
                return;
            }

            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->getApiEndpointUrl(),
                CURLOPT_RETURNTRANSFER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($post),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer {$this->apiKey}",
                ),
            ));

            $response = curl_exec($curl);

            if (curl_errno($curl))
                return curl_error($curl);

            return $response;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    private function getApiEndpointUrl()
    {
        $url = null;

        if ($this->env == 'live')
            $url = 'http://hyperscout.local/api/capture/search';
        if ($this->env == 'dev')
            $url = 'https://scout.hyperzod.dev/api/capture/search';
        if ($this->env == 'local')
            $url = 'http://hyperscout.local/api/capture/search';

        return $url;
    }

    public function setEnv($env)
    {
        $this->env = $env;
    }

    public function setDebug(bool $val)
    {
        $this->debug = $val;
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }

    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    public function getPlatform()
    {
        return $this->deviceDetect->getDeviceName();
    }

    public function getOs()
    {
        return $this->deviceDetect->getOs('name');
    }

    public function getBrowser()
    {
        return $this->deviceDetect->getClient('name');
    }

    public function getIp()
    {
        if (!is_null($this->ip))
            return $this->ip;
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];
        if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];
        if (isset($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];
        if (isset($_SERVER['REMOTE_ADDR']))
            return $_SERVER['REMOTE_ADDR'];

        return false;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function setHttpUserAgent($userAgentHeader)
    {
        $this->httpUserAgent = $userAgentHeader;
    }
}
